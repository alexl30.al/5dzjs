"use strict";

function createNewUser() {
  this.firstName = prompt("Enter your first name: ", "");

  this.lastName = prompt("Enter your last name", "");
  this.birthday = prompt("Enter a date of your birthday", "dd.mm.yyyy");
  this.birthday = this.birthday.split(".").reverse().join(".");

  this.getAge = function () {
    const date1 = new Date(this.birthday);
    const date2 = new Date(Date.now());
    let age;
    age = (date2 - date1) / (1000 * 60 * 60 * 24 * 365);
    return Math.trunc(age);
  };
  this.getPassword = function () {
    let newPasss =
      this.firstName.charAt(0).toUpperCase() +
      this.lastName.toLowerCase() +
      this.birthday.substr(0, 4);
    return newPasss;
  };

  this.getLogin = function () {
    let newLogin =
      this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
    return newLogin;
  };
}

let newUserObj = new createNewUser();
console.log(`Your login is: ${newUserObj.getLogin()}`);
console.log(`Your Age is: ${newUserObj.getAge()}`);
console.log(`Your Password is: ${newUserObj.getPassword()}`);
